const fetch = require("node-fetch");
const tokenContractAbi = require("../artifacts/contracts/bewToken.sol/bewToken.json");
const nftContractAbi = require("../artifacts/contracts/bewNFT.sol/bewNFT.json");
const auctionContractAbi = require("../artifacts/contracts/bewAuction.sol/bewAuction.json");

const contractDetails = require("../contract_details.json");
const dotenv = require('dotenv');
dotenv.config();
const authToken = process.env.BICONOMY_AUTH_TOKEN;
const apiKey = process.env.BICONOMY_DAPP_API_KEY;
const scuri = "https://api.biconomy.io/api/v1/smart-contract/public-api/addContract";
const methoduri = "https://api.biconomy.io/api/v1/meta-api/public-api/addMethod";
var tokenAbi = JSON.stringify(tokenContractAbi.abi);
var nftAbi = JSON.stringify(nftContractAbi.abi);
var auctionAbi = JSON.stringify(auctionContractAbi.abi);

const tokenFormData = new URLSearchParams({
  "contractName" : "bewToken",
  "contractAddress" : contractDetails.tokenContract,
  "abi" : tokenAbi,
  "contractType" : "SC",
  "metaTransactionType": "TRUSTED_FORWARDER"
});
const nftFormData = new URLSearchParams({
  "contractName" : "bewNFT",
  "contractAddress" : contractDetails.nftContract,
  "abi" : nftAbi,
  "contractType" : "SC",
  "metaTransactionType": "TRUSTED_FORWARDER"
});
const auctionFormData = new URLSearchParams({
  "contractName" : "bewAuction",
  "contractAddress" : contractDetails.auctionContract,
  "abi" : auctionAbi,
  "contractType" : "SC",
  "metaTransactionType": "TRUSTED_FORWARDER"
});

const makeRequest = async (formData, url) => {
  const requestOptions = {
    method: 'POST',
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
      "authToken": authToken,
      "apiKey" : apiKey
    },
    body: formData
  };

  const res = await fetch(url, requestOptions);
  res.status != 200 ? console.log(res) : () => {};
}
const createContracts = async () => {
  await makeRequest(tokenFormData, scuri);
  await makeRequest(nftFormData, scuri);
  await makeRequest(auctionFormData, scuri);
}

createContracts();

const approveBew = new URLSearchParams({
  "apiType" : "native",
  "methodType" : "write",
  "name": "approveBew",
  "contractAddress" : contractDetails.tokenContract,
  "method" : "approve"
});

const mintNFT =  new URLSearchParams({
  "apiType" : "native",
  "methodType" : "write",
  "name": "mintNFT",
  "contractAddress" : contractDetails.nftContract,
  "method" : "mint"
});
const approveNFT =  new URLSearchParams({
  "apiType" : "native",
  "methodType" : "write",
  "name": "approveNFT",
  "contractAddress" : contractDetails.nftContract,
  "method" : "approve"
});
const transfer = new URLSearchParams({
  "apiType" : "native",
  "methodType" : "write",
  "name": "transferBew",
  "contractAddress" : contractDetails.tokenContract,
  "method" : "transfer"
});
const createAuction = new URLSearchParams({
  "apiType" : "native",
  "methodType" : "write",
  "name": "createAuction",
  "contractAddress" : contractDetails.auctionContract,
  "method" : "createAuction"
});
const startAuction = new URLSearchParams({
  "apiType" : "native",
  "methodType" : "write",
  "name": "startAuction",
  "contractAddress" : contractDetails.auctionContract,
  "method" : "startAuction"
});
const bidAuction = new URLSearchParams({
  "apiType" : "native",
  "methodType" : "write",
  "name": "bidAuction",
  "contractAddress" : contractDetails.auctionContract,
  "method" : "bid"
});
const endAuction = new URLSearchParams({
  "apiType" : "native",
  "methodType" : "write",
  "name": "endAuction",
  "contractAddress" : contractDetails.auctionContract,
  "method" : "end"
});

const createMethods = async () => {
  await makeRequest(approveBew, methoduri);
  await makeRequest(approveNFT, methoduri);
  await makeRequest(mintNFT, methoduri);
  await makeRequest(transfer, methoduri);
  await makeRequest(createAuction, methoduri);
  await makeRequest(startAuction, methoduri);
  await makeRequest(bidAuction, methoduri);
  await makeRequest(endAuction, methoduri);
}

createMethods();
/*
var formData = new URLSearchParams({
  "contractName" : "bewToken",
  "contractAddress" : contractDetails.tokenContract,
  "abi" : tokenAbi,
  "contractType" : "SC",
  "metaTransactionType": "TRUSTED_FORWARDER"
});


const requestOptions = {
  method: 'POST',
  headers: {
    "Content-Type": "application/x-www-form-urlencoded",
    "authToken": authToken,
    "apiKey" : apiKey
  },
  body: formData
};


fetch(url, requestOptions)
  .then(response => response.json())
  .then(data => console.log(data))
  .catch(error => console.error('Error:', error));*/
