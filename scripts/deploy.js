const hre = require("hardhat");
const fs = require('fs');
async function main() {
  try{
  const tokenContractFactory = await hre.ethers.getContractFactory("bewToken");
  const nftContractFactory = await hre.ethers.getContractFactory("bewNFT");
  const auctionContractFactory = await hre.ethers.getContractFactory("bewAuction");
  const totalSupply = 1000000000000000000n * BigInt(1000000) * BigInt(process.env.TOTALSUPPLY_IN_MILLION);
  console.log(`Deploying token \n forwarder : ${process.env.BICONOMY_FORWARDER}\n supply : ${totalSupply/BigInt(1000 * 1000)}m`);
  const tokenContract = await tokenContractFactory.deploy(
    'BewCoin',
    'BEW',
    process.env.BICONOMY_FORWARDER,
    totalSupply
  );
    console.log("token contract deployed");
  const nftContract = await nftContractFactory.deploy(process.env.BICONOMY_FORWARDER);
  //await tokenContract.deployed();

//  await nftContract.deployed();
  console.log("nft contract deployed");
  const auctionContract = await auctionContractFactory.deploy(
    process.env.BICONOMY_FORWARDER,
    tokenContract.address,
    10
  );
//  await auctionContract.deployed();
  console.log("Auction contract deployed");
  const contracts = {
    "auctionContract" : auctionContract.address,
    "tokenContract" : tokenContract.address,
    "nftContract" : nftContract.address
  };
  let data = JSON.stringify(contracts);
  fs.writeFileSync("contract_details.json",data);
} catch(err) {
  console.log(err);
}
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
