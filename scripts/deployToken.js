const hre = require("hardhat");

async function main() {
  const contractFactory = await hre.ethers.getContractFactory("bewToken");
  //hard coded the parameters
  //can be changed to be taken from process.argv
  const contract = await contractFactory.deploy(
    'BewCoin', 'BEW', "0x9399BB24DBB5C4b782C70c2969F58716Ebbd6a3b", 1000000000000000000n * 1000 * 1000 * 52 // 52 million
  );

  await contract.deployed();

  console.log("Contract deployed to:", contract.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
