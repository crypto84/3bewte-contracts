const { expect } = require("chai");
const hre = require("hardhat");

const totalSupply = 1000000000000000000n * BigInt(1000 * 1000 * 52);
const forwarder = "0x9399BB24DBB5C4b782C70c2969F58716Ebbd6a3b";
const ipfsUrl = "https://ipfs.io/ipfs/bafyreift6sbigsst7424jyzfjh5vtwrthgxbzrvgoh5tuureyw25qcwfki/metadata.json";
let bewContract = null;
let nftContract = null;
let auctionContract = null;
let owner;
let creator;
let bidder;

describe("All contracts must be deployed", async function(){
  it("NFT Contract deployment", async function(){
    const contractFactory = await hre.ethers.getContractFactory("bewNFT");
    nftContract = await contractFactory.deploy(forwarder);
  });
  it("BEW Contract deployment with dummy trusted forwarder", async function(){
    [owner, creator, bidder] = await ethers.getSigners();
    const contractFactory = await hre.ethers.getContractFactory("bewToken");
    bewContract = await contractFactory.deploy(
      'BewCoin', 'BEW', forwarder, totalSupply// 52 million
      );
    expect(await bewContract.balanceOf(owner.address)).to.equal(totalSupply);
  });

  it("Auction Contract deployment with the deployed bew contract", async function(){
    const contractFactory = await hre.ethers.getContractFactory("bewAuction");
    auctionContract = await contractFactory.deploy(forwarder, bewContract.address, 10);
  });
})

describe("NFT Contract Deployment and setup", function(){
  it("Should mint one NFT", async function(){
    const res = await nftContract.connect(creator).mint(ipfsUrl);
    expect(await nftContract.ownerOf(1)).to.equal(creator.address);
  })
  it("Approve the NFT to Auction Contract", async function(){
    const res = await nftContract.connect(creator).approve(auctionContract.address, 1);
    expect(await nftContract.getApproved(1)).to.equal(auctionContract.address);
  })
})

describe("Auction Contract", function(){
  it("Should fail when someone else creates an auction", async function(){
    const tryCreatingAuctionWithSomeoneElse = async () => {
      const res = await auctionContract.connect(owner).createAuction(nftContract.address, 1, 100);
    }

    expect(tryCreatingAuctionWithSomeoneElse).to.throw
  })
  it("Should create an auction", async function(){
    expect(await nftContract.getApproved(1)).to.equal(auctionContract.address);
    await auctionContract.connect(creator).createAuction(nftContract.address, 1, 100);
  })
})

describe("Bidding", function(){
  it("Should transfer bews to bidding account", async function(){
    await bewContract.connect(owner).transfer(bidder.address, 200);
    expect(await bewContract.balanceOf(bidder.address)).to.equal(200);
  });
  it("Should approve auction contract to escrow money", async function(){
    await bewContract.connect(bidder).approve(auctionContract.address, 120);
    expect(await bewContract.allowance(bidder.address, auctionContract.address)).to.equal(120);
  })
  it("Should start auction from creator", async function(){
    await auctionContract.connect(creator).startAuction(nftContract.address, 1, 86400);
  })
  it("Should fail if lower bid is made", async function(){
    async function createFailingBid(){
      await auctionContract.connect(bidder).bid(nftContract.address, 1, 50);
    }
    expect(createFailingBid).to.throw
  });
  it("Should create a new bid", async function(){
    await auctionContract.connect(bidder).bid(nftContract.address, 1, 110);
    expect(await bewContract.balanceOf(bidder.address)).to.equal(90);
  });
  it("Should reimburse old bidder if new bid is made", async function(){
    await bewContract.connect(owner).approve(auctionContract.address, 120);
    await auctionContract.connect(owner).bid(nftContract.address, 1, 115);
    expect(await bewContract.balanceOf(bidder.address)).to.equal(200);
    //expect(BigInt(await bewContract.balanceOf(owner.address))).to.equal(totalSupply - BigInt(115));
  });
  it("Should end a bid and transfer assets", async function(){
    await auctionContract.connect(creator).end(nftContract.address, 1);
    const res = await bewContract.balanceOf(creator.address);
    expect(res).to.equal(BigInt(Math.ceil(115 * 0.9)));
    expect(await nftContract.ownerOf(1)).to.equal(owner.address);
  });
})

describe("No bidding", function(){
  it("Should revert the nft back to creator if no bids are made", async function(){
    const res = await nftContract.connect(creator).mint(ipfsUrl);
    await nftContract.connect(creator).approve(auctionContract.address, 2);
    await auctionContract.connect(creator).createAuction(nftContract.address, 2, 100);
    await auctionContract.connect(creator).startAuction(nftContract.address, 2, 86400);
    await auctionContract.connect(creator).end(nftContract.address, 2);
    expect(await nftContract.ownerOf(2)).to.equal(creator.address);
  });

})
//need to write more concise & detailed tests
