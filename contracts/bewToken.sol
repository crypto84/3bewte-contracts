// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.1;

import "@opengsn/contracts/src/BaseRelayRecipient.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract bewToken is BaseRelayRecipient, ERC20 {

  constructor(string memory name_, string memory symbol_, address forwarder_, uint256 amount)
    ERC20(name_, symbol_) {
    _setTrustedForwarder(forwarder_);
    _mint(_msgSender(), amount);
  }

  string public override versionRecipient = "2.2.0";

  function _msgSender() internal view override(Context, BaseRelayRecipient)
      returns (address sender) {
      sender = BaseRelayRecipient._msgSender();
  }

  function _msgData() internal view override(Context, BaseRelayRecipient)
      returns (bytes memory) {
      return BaseRelayRecipient._msgData();
  }
}
