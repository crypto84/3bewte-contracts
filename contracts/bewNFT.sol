// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.1;

import "@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@opengsn/contracts/src/BaseRelayRecipient.sol";

contract bewNFT is BaseRelayRecipient, ERC721URIStorage, Ownable {
  uint256 currentTokenCount;
  event Mint(address indexed _from, uint256 _tokenId);
  constructor(address _trustedForwarder) ERC721 ("Bew NFT", "BEW") {
    _setTrustedForwarder(_trustedForwarder);
    currentTokenCount = 0;
  }

  function _msgSender() internal override(Context, BaseRelayRecipient) virtual view returns (address ret) {
      ret = BaseRelayRecipient._msgSender();
  }
  function _msgData() internal override(Context, BaseRelayRecipient) virtual view returns (bytes calldata ret) {
      return BaseRelayRecipient._msgData();
  }
  function versionRecipient() external override virtual view returns (string memory) {
    return "0.1.1";
  }
  function mint(string calldata _uri) external {
    currentTokenCount = currentTokenCount + 1;
    super._mint(_msgSender(), currentTokenCount);
    super._setTokenURI(currentTokenCount, _uri);
    emit Mint(_msgSender(), currentTokenCount);
  }
}
