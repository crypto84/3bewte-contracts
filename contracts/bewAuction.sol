// SPDX-License-Identifier: MIT
pragma solidity ^0.8;

import "./bewToken.sol";
import "@openzeppelin/contracts/token/ERC721/IERC721.sol";
import "@opengsn/contracts/src/BaseRelayRecipient.sol";
import "hardhat/console.sol";

contract bewAuction is BaseRelayRecipient {
    address _master3bewteAddress;
    uint8 _percentage;
    event Start();
    event Bid(address indexed sender, uint amount);
    event RevertBid(address indexed bidder, uint amount);
    event End(address winner, uint amount);

    IERC721 public nft;
    bewToken private token;
    uint public nftId;

    address public seller;
    uint public endAt;
    bool public started;
    bool public ended;

    address public highestBidder;
    uint256 public highestBid;
    struct Auction {
      address seller;
      uint256 askingPrice;
      uint256 endAt;
      uint256 startAt;
      uint256 highestBid;
      address highestBidder;
      bool live;
    }
    mapping(address => mapping(uint256 => Auction)) public nftAuctions;
    constructor(
        address forwarder_,
        address bewTokenAddr,
        uint8 percentage_
    ) {
      _setTrustedForwarder(forwarder_);
      token = bewToken(bewTokenAddr);
      _master3bewteAddress = _msgSender();
      _percentage = percentage_;
    }
    function versionRecipient() external virtual override view returns (string memory) {
      return "v0.0.1";
    }
    function changePercentage(uint8 newPercentage) external {
      require(_msgSender() == _master3bewteAddress, "only owner can change this");
      _percentage = newPercentage;
    }
    function createAuction(address _nft, uint256 _tokenId, uint256 _price) external {
      nft = IERC721(_nft);
      require(nft.ownerOf(_tokenId) == _msgSender(), "E002");
      nft.transferFrom(_msgSender(), address(this), _tokenId);
      nftAuctions[_nft][_tokenId].askingPrice = _price;
      nftAuctions[_nft][_tokenId].seller = _msgSender();
      nftAuctions[_nft][_tokenId].live = false;
    }

    function startAuction(address _nft, uint256 _tokenId, uint32 duration) external {
      require(nftAuctions[_nft][_tokenId].seller == _msgSender(), "E002");
      require(!nftAuctions[_nft][_tokenId].live, "E001");
      nftAuctions[_nft][_tokenId].live = true;
      nftAuctions[_nft][_tokenId].startAt = block.timestamp;
      nftAuctions[_nft][_tokenId].endAt = block.timestamp + duration;
      emit Start();
    }

    function bid(address _nft, uint256 _tokenId, uint256 value) external {
        require(nftAuctions[_nft][_tokenId].live, "E003"); // Auction not live
        require(block.timestamp < nftAuctions[_nft][_tokenId].endAt, "E003");
        require(value > nftAuctions[_nft][_tokenId].highestBid, "E004");
        if(nftAuctions[_nft][_tokenId].highestBidder != address(0)){
          token.transfer(nftAuctions[_nft][_tokenId].highestBidder, nftAuctions[_nft][_tokenId].highestBid);
          emit RevertBid(nftAuctions[_nft][_tokenId].highestBidder, nftAuctions[_nft][_tokenId].highestBid);
        }
        token.transferFrom(_msgSender(), address(this), value);

        nftAuctions[_nft][_tokenId].highestBidder = _msgSender();
        nftAuctions[_nft][_tokenId].highestBid = value;

        emit Bid(_msgSender(), value);
    }

    function end(address _nft, uint256 _tokenId) external {
        require(nftAuctions[_nft][_tokenId].live && nftAuctions[_nft][_tokenId].endAt >= block.timestamp, "E003");
        require(nftAuctions[_nft][_tokenId].seller == _msgSender(), "E002");
        ended = true;
        if (nftAuctions[_nft][_tokenId].highestBidder != address(0)) {
            nft.safeTransferFrom(address(this), nftAuctions[_nft][_tokenId].highestBidder, _tokenId);
            uint256 commission = nftAuctions[_nft][_tokenId].highestBid * _percentage/100;
            token.transfer(nftAuctions[_nft][_tokenId].seller, nftAuctions[_nft][_tokenId].highestBid - commission);
            token.transfer(_master3bewteAddress, commission);
        } else {
            nft.safeTransferFrom(address(this), nftAuctions[_nft][_tokenId].seller, _tokenId);
        }

        emit End(nftAuctions[_nft][_tokenId].highestBidder, nftAuctions[_nft][_tokenId].highestBid);
    }
}
